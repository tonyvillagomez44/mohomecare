﻿using MOHomeCareApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MOHomeCareApp.Models
{
    public class File
    {
        public int id { get; set; }
        public string path { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
        public int size { get; set; }
        public int rows { get; set; }
        public int columns { get; set; }
        public int selected { get; set; }
        
        string conString = ConfigurationManager.ConnectionStrings["MOHomeCareConnection"].ConnectionString;

        public dynamic DumpFileInformationToDataSet(FormCollection form)
        {
            DataSet dataSet;
            DataTable dtUploadsData;
            DataRow row;

            dataSet = new DataSet();
            dtUploadsData= this.BuildUploadsData();
            dataSet.Tables.Add(dtUploadsData);

            mohomecareEntities mo = new mohomecareEntities();
            List<int> idList = new List<int>();
            string[] text;
            foreach (var id in form)
            {
                idList.Add(Int32.Parse(id.ToString()));
            }
            var selFiles = 
                    mo.UploadsAvailable
                    .Where(f => idList.Contains(f.id))
                    .ToList();

            foreach (var selected in selFiles)
            {
                text = System.IO.File.ReadAllLines(@"" + selected.file_path.ToString());
                
                foreach(var dataRow in text)
                {
                    var reader = dataRow.Split('\t');
                
                    row = dtUploadsData.NewRow();

                    row["UploadId"] = Int32.Parse(selected.id.ToString());
                    row["UploadName"] = selected.file_name;
                    row["LastName"] = reader[0];
                    row["FirstName"] = reader[1];
                    row["MiddleName"] = reader[2];
                    row["EmployeeId"] = reader[3];
                    row["LastSSN"] = reader[4];
                    row["HomeAddress"] = reader[5];
                    row["HomeCity"] = reader[6];
                    row["HomeState"] = reader[7];
                    row["HomeZip"] = reader[8];
                    row["HomePhone"] = reader[9];
                    row["HoursWorked"] = reader[10];
                    row["GrossPay"] = reader[11];
                    row["UnionDues"] = reader[12];
                    row["CareDues"] = reader[13];

                    dtUploadsData.Rows.Add(row); 
                }
            }

            //insert file data in database
            using(SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                SqlCommand command = new SqlCommand("delete from files.uploadsdata", con);
                command.ExecuteNonQuery();
                using(SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                {
                    sqlBulkCopy.DestinationTableName = "files.UploadsData";
                    
                    sqlBulkCopy.ColumnMappings.Add("UploadId", "upload_id");
                    sqlBulkCopy.ColumnMappings.Add("UploadName", "upload_name");
                    sqlBulkCopy.ColumnMappings.Add("LastName", "last_name");
                    sqlBulkCopy.ColumnMappings.Add("FirstName", "first_name");
                    sqlBulkCopy.ColumnMappings.Add("MiddleName", "middle_name");
                    sqlBulkCopy.ColumnMappings.Add("EmployeeId", "employee_id");
                    sqlBulkCopy.ColumnMappings.Add("LastSSN", "last_ssn");
                    sqlBulkCopy.ColumnMappings.Add("HomeAddress", "home_address");
                    sqlBulkCopy.ColumnMappings.Add("HomeCity", "home_city");
                    sqlBulkCopy.ColumnMappings.Add("HomeState", "home_state");
                    sqlBulkCopy.ColumnMappings.Add("HomeZip", "home_zip");
                    sqlBulkCopy.ColumnMappings.Add("HomePhone", "home_phone");
                    sqlBulkCopy.ColumnMappings.Add("HoursWorked", "hours_worked");
                    sqlBulkCopy.ColumnMappings.Add("GrossPay", "gross_pay");
                    sqlBulkCopy.ColumnMappings.Add("UnionDues", "union_dues");
                    sqlBulkCopy.ColumnMappings.Add("CareDues", "care_dues");
                    
                    sqlBulkCopy.WriteToServer(dataSet.Tables["UploadsData"]);
                }
                con.Close();
            }
            return selFiles;
        }

        public string[] Read()
        {
            string[] text = System.IO.File.ReadAllLines(@"C:\FilesMOHomeCare\VendorName_PayPeriod.txt");
            return text;
        }

        public dynamic GetListOfFilesToProcess()
        {
            //method to get a list of files available to process dues
            mohomecareEntities mo = new mohomecareEntities();
            var list = mo.UploadsAvailable.ToList();
            return list;
        }

        public dynamic UpdateFileProcessHistory(FormCollection form)
        {
            //method to insert filenames selected by the user to process it
            DataSet dataSet;
            DataTable dtFilesProcess;
            DataRow row;
            
            List<int> idList = new List<int>();
            foreach (var id in form)
            {
                idList.Add(Int32.Parse(id.ToString()));
            }
            mohomecareEntities mo = new mohomecareEntities();
            
            //update table and set tru fopr the files selected by the user
            var updateId = mo.UploadsAvailable.Where(f => idList.Contains(f.id)).ToList();
            updateId.ForEach(f => f.file_process = true);
            mo.SaveChanges();

            var list =
                mo.UploadsAvailable
                    .Where(t => idList.Contains(t.id))
                    .ToList();

            dataSet = new DataSet();
            dtFilesProcess = this.BuildFileListTable();
            dataSet.Tables.Add(dtFilesProcess);

            //dump file selectd information in a datatable
            foreach(var selected in list)
            {
                row = dtFilesProcess.NewRow();
                row["FileId"] = Int32.Parse(selected.id.ToString());
                row["FilePath"] = selected.file_path;
                row["FileName"] = selected.file_name;
                row["FilePostedDate"] = selected.file_date;
                row["FileSize"] = Int32.Parse(selected.file_size.ToString());
                row["TotalLines"] = Int32.Parse(selected.file_rows.ToString());
                row["TotalColumns"] = Int32.Parse(selected.file_column.ToString());
                row["ForProcessing"] = 1;
                row["FileProcessDate"] = selected.file_date;
                row["UserId"] = 1;
                dtFilesProcess.Rows.Add(row);
            }
            
            //do a bulk insert to file processing history selected by the user
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                if (list.Count > 0)
                {
                    using(SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        sqlBulkCopy.DestinationTableName = "files.UploadsProcessingHistory";
                        sqlBulkCopy.ColumnMappings.Add("FileId", "id");
                        sqlBulkCopy.ColumnMappings.Add("FilePath", "file_path");
                        sqlBulkCopy.ColumnMappings.Add("FileName", "file_name");
                        sqlBulkCopy.ColumnMappings.Add("FilePostedDate", "file_date");
                        sqlBulkCopy.ColumnMappings.Add("FileSize", "file_size");
                        sqlBulkCopy.ColumnMappings.Add("TotalLines", "file_rows");
                        sqlBulkCopy.ColumnMappings.Add("TotalColumns", "file_column");
                        sqlBulkCopy.ColumnMappings.Add("ForProcessing", "file_process");
                        sqlBulkCopy.ColumnMappings.Add("FileProcessDate", "file_time");
                        sqlBulkCopy.ColumnMappings.Add("UserId", "user_id");

                        sqlBulkCopy.WriteToServer(dataSet.Tables["ListFilesTable"]);
                        con.Close();
                    }
                }
            }

            return list;
        }

        public bool InsertUploadsAvailables(DataSet dsFilesAvailables)
        {
            //method to insert in database file names in folder to be processed for the first time --STEP#2--
            if(dsFilesAvailables.Tables["ListFilesTable"].Rows.Count>0)
            {
                using(SqlConnection con = new SqlConnection(conString))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand("delete from files.UploadsAvailable", con);
                    if (command.ExecuteNonQuery()>-1)
                    {
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                        {
                            sqlBulkCopy.DestinationTableName = "files.UploadsAvailable";
                            sqlBulkCopy.ColumnMappings.Add("FilePath", "file_path");
                            sqlBulkCopy.ColumnMappings.Add("FileName", "file_name");
                            sqlBulkCopy.ColumnMappings.Add("FilePostedDate", "file_date");
                            sqlBulkCopy.ColumnMappings.Add("FileSize", "file_size");
                            sqlBulkCopy.ColumnMappings.Add("TotalLines", "file_rows");
                            sqlBulkCopy.ColumnMappings.Add("TotalColumns", "file_column");
                            sqlBulkCopy.ColumnMappings.Add("ForProcessing", "file_process");

                            sqlBulkCopy.WriteToServer(dsFilesAvailables.Tables["ListFilesTable"]);
                            con.Close();
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public DataSet GetFilesInFolder()
        {
            //method to read list files available for processing in C:\FilesMOHomeCare --STEP#1--
            DataSet dataSet;
            DataTable dtFilesList;
            DataRow row;

            dataSet = new DataSet();
            dtFilesList = this.BuildFileListTable();
            dataSet.Tables.Add(dtFilesList);

            string[] files = Directory.GetFiles(@"C:\FilesMOHomeCare\", "*.txt");
            for (int i = 0; i < files.Length; i++)
            {
                row = dtFilesList.NewRow();
                string columns = System.IO.File.ReadLines(@"" + Path.GetFullPath(files[i])).Skip(0).Take(1).First();
                string[] totColumns = columns.Split('\t');

                row["FilePath"] = Path.GetFullPath(files[i]);
                row["FileName"] = Path.GetFileName(files[i]);
                row["TotalLines"] = System.IO.File.ReadAllLines(@"" + Path.GetFullPath(files[i])).Count();
                row["FilePostedDate"] = System.IO.File.GetLastWriteTime(@""+Path.GetFullPath(files[i]));
                row["TotalColumns"] = totColumns.Count();
                row["FileSize"] = System.IO.File.ReadAllBytes(@"" + Path.GetFullPath(files[i])).Length;
                row["ForProcessing"] = 0;

                dtFilesList.Rows.Add(row);
            }

            //delete rows on table with files available
            using (SqlConnection con = new SqlConnection(conString))
            {
                con.Open();
                using(SqlCommand cmd = new SqlCommand("delete from files.UploadsAvailable", con))
                {
                    if(cmd.ExecuteNonQuery() > -1)
                        return dataSet; 
                }
                con.Close();
            }
            return dataSet;
        }

        public DataTable BuildUploadsData()
        {
            DataTable dtUploadsData;
            DataColumn column;

            dtUploadsData = new DataTable("UploadsData");

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "UploadId";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "UploadName";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "LastName";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FirstName";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "MiddleName";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "EmployeeId";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "LastSSN";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "HomeAddress";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "HomeCity";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "HomeState";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "HomeZip";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "HomePhone";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "HoursWorked";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "GrossPay";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "UnionDues";
            dtUploadsData.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CareDues";
            dtUploadsData.Columns.Add(column);

            return dtUploadsData;
        }

        public DataTable BuildFileListTable()
        {
            DataTable dtFilesList;
            DataColumn column;

            dtFilesList = new DataTable("ListFilesTable");

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FileId";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FilePath";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FileName";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TotalLines";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FilePostedDate";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FileProcessDate";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "TotalColumns";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FileSize";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ForProcessing";
            dtFilesList.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "UserId";
            dtFilesList.Columns.Add(column);

            return dtFilesList;
        }
    }
}