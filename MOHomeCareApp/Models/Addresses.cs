﻿using MOHomeCareApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MOHomeCareApp.Models
{
    public class Addresses
    {
        string conString = ConfigurationManager.ConnectionStrings["MOHomeCareConnection"].ConnectionString;

        public dynamic getAllStates()
        {
            mohomecareEntities mo = new mohomecareEntities();
            var states =
                mo.State
                    .Where(f => f.id == 1)
                    .Select(f => new { f.id, f.name });
            return states;
        }

        public List<WorkerDataEntry> getAllCities(string term)
        {
            mohomecareEntities mo = new mohomecareEntities();
            return
                mo.City
                    .Where(f => f.state_id == 1 && f.name.Contains(term))
                    .Select(f => new WorkerDataEntry() { vmHomeCityId = f.id, vmHomeCity = f.name }).ToList();
        }

        public List<ListOfZips> getAllZips(string term)
        {
            mohomecareEntities mo = new mohomecareEntities();
            return
                mo.Zip
                    .Where(f => f.state_id == 1 && f.name.Contains(term))
                    .Select(f => new ListOfZips() { zipId = f.id}).ToList();
        }

        public IEnumerable<WorkerDataEntry> getSelectListOfStates()
        {
            mohomecareEntities mo = new mohomecareEntities();

            return
                mo.State
                    .Select(u => new WorkerDataEntry
                    {
                        vmHomeStateId = u.id,
                        vmHomeStateName = u.name
                    }).ToList();
        }
    }
}