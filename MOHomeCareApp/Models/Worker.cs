﻿using MOHomeCareApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MOHomeCareApp.Models
{
    public class Worker
    {
        public WorkerSummaryReport WorkerListLanguageInformation(int id)
        {
            mohomecareEntities mo = new mohomecareEntities();
            WorkerSummaryReport w = new WorkerSummaryReport();
            w = this.WorkerListGlobalInformation(id);
            w.sumLangList = mo.SummaryLanguageByWorker(id).ToList();
            
            return w;
        }

        public WorkerSummaryReport WorkerListEmailInformation(int id)
        {
            mohomecareEntities mo = new mohomecareEntities();
            WorkerSummaryReport w = new WorkerSummaryReport();
            w = this.WorkerListGlobalInformation(id);
            w.sumEmailList = mo.SummaryEmailByWorker(id).ToList();

            return w;
        }

        public WorkerSummaryReport WorkerListPhoneInformation(int id)
        {
            mohomecareEntities mo = new mohomecareEntities();
            WorkerSummaryReport w = new WorkerSummaryReport();
            w = this.WorkerListAddressInformation(id);
            w.sumPhoneList = mo.SummaryPhoneByWorker(id).ToList();
           
            return w;
        }

        public WorkerSummaryReport WorkerListAddressInformation(int id)
        {
            mohomecareEntities mo = new mohomecareEntities();
            WorkerSummaryReport w = new WorkerSummaryReport();
            w = this.WorkerListGlobalInformation(id);
            w.sumAddressList = mo.SummaryAddressByWorker(id).ToList();

            return w;
        }
        
        public WorkerSummaryReport WorkerListGlobalInformation(int id)
        {
            mohomecareEntities mo = new mohomecareEntities();
            WorkerSummaryReport w = new WorkerSummaryReport();
            w.sumWorkerList = mo.SummaryListByWorker(id).ToList();

            foreach (var data in w.sumWorkerList)
            {
                w.FirstName = data.firstname;
                w.WorkerId = data.id;
                w.SSN = data.ssn;
                w.VanId = data.van_id;
                w.LastName = data.lastname;
                w.MiddleName = data.middlename;
                w.LastUpdate = data.lastupdate;
            }

            return w;
        }

        public List<SearchWorkers_Result> SearchWorker(WorkerSearch form)
        {
            mohomecareEntities mo = new mohomecareEntities();
            return mo.SearchWorkers(form.vmFirstName, form.vmLastName, form.vmSSN, form.vmHomePhone, form.vmHomeAddress).ToList();
        }

        public dynamic InsertNewWorker(WorkerDataEntry form)
        {
            mohomecareEntities mo = new mohomecareEntities();
            List w = new List();
            Address a = new Address();
            Phone p = new Phone();
            Phone mob = new Phone();
            Email e = new Email();
            Language l = new Language();
            Card c = new Card();
            Defaults d = new Defaults();

            try
            {
                /*Insert Worker*/
                if (form.vmLastName != null)
                    w.lastname = form.vmLastName.ToString().Trim();
                if (form.vmFirstName != null)
                    w.firstname = form.vmFirstName.ToString().Trim();
                if (form.vmMiddleName != null)
                    w.middlename = form.vmMiddleName.ToString().Trim();
                if (form.vmDOBDay != null && form.vmDOBMonth != null && form.vmDOBYear != null)
                    w.birthdate = DateTime.Parse(form.vmDOBMonth.ToString().Trim()
                        + "/" + form.vmDOBDay.ToString().Trim()
                        + "/" + form.vmDOBYear.ToString().Trim());
                if (form.vmSSN != null)
                    w.ssn = form.vmSSN.ToString().Trim();
                if (form.vmHomeRegVote)
                    w.registertovote = form.vmHomeRegVote;
                if(form.vmDataSourceId != null)
                    w.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                if (form.vmVanId != null)
                    w.van_id = form.vmVanId;
                w.lastupdate = DateTime.Now;
                w.entereddate = DateTime.Now;
                
                mo.List.Add(w);
                mo.SaveChanges();
                
                var insertId = w.id;
                d.worker_id = insertId;
                mo.Defaults.Add(d);
                //mo.SaveChanges();

                if(insertId > 0)
                {
                    /*Insert Worker Address*/
                    if (form.vmHomeAddress != null)
                    {
                        a.worker_id = insertId;
                        a.type_id = 1;
                        a.flag_id = 1;
                        a.street = form.vmHomeAddress.ToString().Trim();
                        a.city_id = Int32.Parse(form.vmHomeCityId.ToString());
                        a.state_id = Int32.Parse(form.vmHomeStateId.ToString());
                        a.zip_id = Int32.Parse(form.vmHomeZip.ToString());
                        a.lastupdate = DateTime.Now;
                        a.entereddate = DateTime.Now;
                        a.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                        a.inactive = false;

                        mo.Address.Add(a);
                        mo.SaveChanges();

                        var AddressId = a.id;
                        mo.Defaults.First(t => t.worker_id == insertId);
                        d.address_id = AddressId;
                        //mo.SaveChanges();
                    }
                    /*Insert Home Phone Number*/
                    if(form.vmHomePhone != null)
                    {
                        p.worker_id = insertId;
                        p.type_id = 2;
                        p.flag_id = 1;
                        p.sms_active = false;
                        p.number_id = form.vmHomePhone.ToString().Trim();
                        p.entered_date = DateTime.Now;
                        p.lastupdate = DateTime.Now;
                        p.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                        p.inactive = false;

                        mo.Phone.Add(p);
                        mo.SaveChanges();
                        
                        var PhoneId = p.id;
                        mo.Defaults.First(t => t.worker_id == insertId);
                        d.phone_id = PhoneId;
                        //mo.SaveChanges();
                    }
                    /*Insert Home Mobile Number*/
                    if (form.vmHomeMobile != null)
                    {
                        mob.worker_id = insertId;
                        mob.type_id = 1;
                        mob.flag_id = 1;
                        mob.sms_active = form.vmHomeMobileSms;
                        mob.number_id = form.vmHomeMobile.ToString().Trim();
                        mob.entered_date = DateTime.Now;
                        mob.lastupdate = DateTime.Now;
                        mob.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                        mob.inactive = false;

                        mo.Phone.Add(mob);
                        //mo.SaveChanges();

                        var MobileId = mob.id;
                        mo.Defaults.First(t => t.worker_id == insertId);
                        d.mobile_id = MobileId;
                        //mo.SaveChanges();
                    }

                    /*Insert Email Address*/
                    if (form.vmHomeEmail != null)
                    {
                        e.worker_id = insertId;
                        e.address = form.vmHomeEmail.Trim();
                        e.type_id = 1;
                        e.flag_id = 1;
                        e.entereddate = DateTime.Now;
                        e.lastupdate = DateTime.Now;
                        e.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                        e.inactive = false;

                        mo.Email.Add(e);
                        mo.SaveChanges();

                        var EmailId = e.id;
                        mo.Defaults.First(t => t.worker_id == insertId);
                        d.email_id = EmailId;
                        //mo.SaveChanges();
                    }

                    /*Insert Language*/
                    if(form.vmHomeLanguageId > 0)
                    {
                        l.worker_id = insertId;
                        l.type_id = Int32.Parse(form.vmHomeLanguageId.ToString());
                        l.flag_id = 1;
                        l.entereddate = DateTime.Now;
                        l.lastupdate = DateTime.Now;
                        l.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                        l.inactive = false;

                        mo.Language.Add(l);
                        mo.SaveChanges();
                        
                        var LanguageId = l.id;
                        mo.Defaults.First(t => t.worker_id == insertId);
                        d.language_id = LanguageId;
                        //mo.SaveChanges();
                    }

                    /*Insert Membership Signed Card*/
                    if(form.vmUnionSignedId == 1) //YES = 1 Check on 'Contact' Class
                    {
                        c.worker_id = insertId;
                        c.type_id = 1; //Membership
                        c.flag_id = 1; //OK
                        c.amount = 0;
                        c.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                        c.inactive = false;
                        c.receiveddate = DateTime.Parse(form.vmUnionReceived.ToString());
                        c.signeddate = DateTime.Parse(form.vmUnionSignedDate.ToString());
                        c.entereddate = DateTime.Now;
                        c.lastupdate = DateTime.Now;

                        mo.Card.Add(c);
                        mo.SaveChanges();

                        var UnionCardId = c.id;
                        mo.Defaults.First(t => t.worker_id == insertId);
                        d.card_id = UnionCardId;
                        //mo.SaveChanges();
                    }

                    /*Insert CARE Signed Card*/
                    if (form.vmCareSignedId == 1) //YES = 1 Check on 'Contact' Class
                    {
                        c.worker_id = insertId;
                        c.type_id = 2; //People
                        c.flag_id = 1; //OK
                        c.amount = form.vmCareAmount;
                        c.source_id = Int32.Parse(form.vmDataSourceId.ToString());
                        c.inactive = false;
                        c.receiveddate = DateTime.Parse(form.vmUnionReceived.ToString());
                        c.signeddate = DateTime.Parse(form.vmUnionSignedDate.ToString());
                        c.entereddate = DateTime.Now;
                        c.lastupdate = DateTime.Now;

                        mo.Card.Add(c);
                        //mo.SaveChanges();
                        
                        var CareCardId = c.id;
                        mo.Defaults.First(t => t.worker_id == insertId);
                        d.care_id = CareCardId;
                        mo.SaveChanges();
                    }

                    mo.SaveChanges();
                }


                return true;
            }
            catch(Exception ex)
            {
                if (ex is SqlException)
                {

                }
                return false;
            }
            

        }

        
    }
}