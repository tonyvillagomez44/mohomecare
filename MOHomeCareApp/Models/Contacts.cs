﻿using MOHomeCareApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MOHomeCareApp.Models
{
    public class Contacts
    {
        public IEnumerable<WorkerDataEntry>getCardSources()
        {
            mohomecareEntities mo = new mohomecareEntities();

            return
                mo.Sources
                    .Select(u => new WorkerDataEntry
                    {
                        vmDataSourceName = u.name,
                        vmDataSourceId = u.id
                    }
                    ).ToList();
        }

        public IEnumerable<WorkerDataEntry> getAllLanguages()
        {
            mohomecareEntities mo = new mohomecareEntities();

            return
                mo.Type3 
                    .Select(u => new WorkerDataEntry
                    {
                        vmHomeLanguageName = u.name,
                        vmHomeLanguageId = u.id
                    }).ToList();
        }

        public SelectList getListOfYesAndNo()
        {
            SelectList yesNoList = new SelectList(new[]
            {
                new SelectListItem {Value = "1", Text = "Yes"},
                new SelectListItem {Value = "2", Text = "No"},
                }, "Value", "Text");

            return yesNoList;
        }

    }
}