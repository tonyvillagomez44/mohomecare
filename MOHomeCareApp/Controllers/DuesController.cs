﻿using MOHomeCareApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MOHomeCareApp.Controllers
{
    [Authorize(Roles = "User")]
    public class DuesController : Controller
    {
        // GET: Dues
        public ActionResult Index()
        {
            DataSet dsFilesAvailable;
            File files = new File();
            
            dsFilesAvailable = files.GetFilesInFolder();
            files.InsertUploadsAvailables(dsFilesAvailable);
            ViewBag.UploadsAvailable = files.GetListOfFilesToProcess();
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            File files = new File();
            ViewBag.UploadsAvailable = files.UpdateFileProcessHistory(form);
            files.DumpFileInformationToDataSet(form);
            return View();
        }

        public ActionResult FileProcessingResults()
        {

            return View();
        }

    }
}