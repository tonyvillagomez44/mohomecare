﻿using MOHomeCareApp.Models;
using MOHomeCareApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MOHomeCareApp.Controllers
{
    [Authorize(Roles = "User")]
    public class WorkersController : Controller
    {
        [HttpGet]
        public ActionResult WorkerSummary(int id, string window)
        {
            WorkerSummaryReport model = new WorkerSummaryReport();
            Worker w = new Worker();
            
            if (window == "_global")
            {
                model = w.WorkerListGlobalInformation(id);
                model.windowsOption = "Global";
            }
            if (window == "_address")
            {
                model = w.WorkerListAddressInformation(id);
                model.windowsOption = "Address";
            }
            if (window == "_phone")
            {
                model = w.WorkerListPhoneInformation(id);
                model.windowsOption = "Phone";
            }
            if (window == "_email")
            {
                model = w.WorkerListEmailInformation(id);
                model.windowsOption = "Email";
            }
            if (window == "_language")
            {
                model = w.WorkerListLanguageInformation(id);
                model.windowsOption = "Language";
            }

            model.windowView = window;
            model.WorkerId = id;

            return View(model);
        }

        public ActionResult SearchResults()
        {
            try
            {
                WorkerSearchResult model = new WorkerSearchResult();
                model.searchResults = (List<SearchWorkers_Result>)TempData["resList"];
                if(model.searchResults.Any())
                {
                    return View(model);
                }
                return RedirectToRoute("Search");
            }
            catch(Exception ex)
            {
                ex.ToString();
                return RedirectToRoute("Search");
            }     
        }

        public ActionResult Search()
        {
            WorkerSearch model = new WorkerSearch();
            return View(model);
        }

        [HttpPost]
        public ActionResult Search(WorkerSearch form)
        {
            Worker w = new Worker();
            var resList = w.SearchWorker(form);
            if(resList.Count() > 0)
            {
                TempData["resList"] = resList;
                return RedirectToRoute("SearchResults");
            }
            else
            return View(form);
        }

        // GET: Workers
        public ActionResult Index()
        {
            Contacts cn = new Contacts();
            Addresses ad = new Addresses();
            var langList = cn.getAllLanguages();
            WorkerDataEntry model  = new WorkerDataEntry();
            model.vmHomeLanguage = new SelectList(cn.getAllLanguages(), "vmHomeLanguageName", "vmHomeLanguageId");
            model.vmHomeState = new SelectList(ad.getSelectListOfStates(), "vmHomeStateId", "vmHomeStateName");
            model.vmMailState = new SelectList(ad.getSelectListOfStates(), "vmHomeStateId", "vmHomeStateName");
            model.vmUnionSigned = new SelectList(cn.getListOfYesAndNo(), "Value", "Text");
            model.vmCareSigned = new SelectList(cn.getListOfYesAndNo(), "Value", "Text");
            model.vmDataSource = new SelectList(cn.getCardSources(), "vmDataSourceId", "vmDataSourceName");
            return View(model);
        }

        [HttpPost]
        public ActionResult Index (WorkerDataEntry form)
        {
            Addresses ad = new Addresses();
            Contacts cn = new Contacts();
            WorkerDataEntry model = new WorkerDataEntry();
            form.vmHomeLanguage = new SelectList(cn.getAllLanguages(), "vmHomeLanguageName", "vmHomeLanguageId");
            form.vmHomeState = new SelectList(ad.getSelectListOfStates(), "vmHomeStateId", "vmHomeStateName");
            form.vmMailState = new SelectList(ad.getSelectListOfStates(), "vmHomeStateId", "vmHomeStateName");
            form.vmUnionSigned = new SelectList(cn.getListOfYesAndNo(), "Value", "Text");
            form.vmCareSigned = new SelectList(cn.getListOfYesAndNo(), "Value", "Text");
            form.vmDataSource = new SelectList(cn.getCardSources(), "vmDataSourceId", "vmDataSourceName");

            if (ModelState.IsValid)
            {
                Worker w = new Worker();
                w.InsertNewWorker(form);
                return View(form);
            }
            return View(form);
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetListOfCities(string term)
        {
            Addresses a = new Addresses();
            var cities = a.getAllCities(term);
            return Json(cities.Select(t => new { id = t.vmHomeCityId, value = t.vmHomeCity, label = t.vmHomeCity }), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetListOfZips(string term)
        {
            Addresses a = new Addresses();
            var zips = a.getAllZips(term);
            return Json(zips.Select(t => new { id = t.zipId, value = t.zipId, label = t.zipId }), JsonRequestBehavior.AllowGet);
        }
    }
}