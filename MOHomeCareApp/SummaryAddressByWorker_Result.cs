//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MOHomeCareApp
{
    using System;
    
    public partial class SummaryAddressByWorker_Result
    {
        public int id { get; set; }
        public int worker_id { get; set; }
        public string addresstype { get; set; }
        public string flagtype { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public int zip_id { get; set; }
        public System.DateTime entereddate { get; set; }
        public System.DateTime lastupdate { get; set; }
        public string sourcecard { get; set; }
        public bool inactive { get; set; }
    }
}
