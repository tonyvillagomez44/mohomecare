﻿using System.Web;
using System.Web.Mvc;
using MOHomeCareApp.Infraestructure;

namespace MOHomeCareApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TransactionFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
