﻿using MOHomeCareApp.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MOHomeCareApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
           var namespaces = new[] { typeof(AuthController).Namespace };
            /*routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );*/

            routes.MapRoute("Home", "", new { controller = "Home", action = "Index" }, namespaces);
            routes.MapRoute("Users", "users", new { controller = "Users", action = "Index" }, namespaces);
            routes.MapRoute("Login", "login", new { controller = "Auth", action = "Login" }, namespaces);
            routes.MapRoute("Logout", "logout", new { controller = "Auth", action = "Logout" }, namespaces);
            routes.MapRoute("Traning", "training", new { controller = "Training", action = "Index" }, namespaces);
            routes.MapRoute("Dues", "dues", new { controller = "Dues", action = "Index" }, namespaces);
            routes.MapRoute("FileProcResults", "fileprocresults", new { controller = "Dues", action = "FileProcessingResults" }, namespaces);


            routes.MapRoute("DataEntry", "dataentry", new { controller = "Workers", action = "Index" }, namespaces);
            routes.MapRoute("Search", "search", new { controller = "Workers", action = "Search" }, namespaces);
            routes.MapRoute("SearchResults", "searchresults", new { controller = "Workers", action = "SearchResults" }, namespaces);
            routes.MapRoute("WorkerSummary", "workersummary", new { controller = "Workers", action = "WorkerSummary" }, namespaces);
            
            //autocomplete function using GET and JQUERY
            routes.MapRoute("GetListOfCities", "GetListOfCities", new { controller = "Workers", action = "GetListOfCities" }, namespaces);
            routes.MapRoute("GetListOfZips", "GetListOfZips", new { controller = "Workers", action = "GetListOfZips" }, namespaces);
        }
    }
}
