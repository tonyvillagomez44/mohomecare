//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MOHomeCareApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class List
    {
        public List()
        {
            this.Address = new HashSet<Address>();
            this.Deductions = new HashSet<Deductions>();
            this.Email = new HashSet<Email>();
            this.Language = new HashSet<Language>();
            this.Phone = new HashSet<Phone>();
            this.Card = new HashSet<Card>();
        }
    
        public int id { get; set; }
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public Nullable<System.DateTime> birthdate { get; set; }
        public string ssn { get; set; }
        public Nullable<bool> registertovote { get; set; }
        public Nullable<System.DateTime> entereddate { get; set; }
        public Nullable<System.DateTime> lastupdate { get; set; }
        public int source_id { get; set; }
        public Nullable<int> van_id { get; set; }
        public Nullable<int> enterprise_id { get; set; }
    
        public virtual Sources Sources { get; set; }
        public virtual ICollection<Address> Address { get; set; }
        public virtual ICollection<Deductions> Deductions { get; set; }
        public virtual ICollection<Email> Email { get; set; }
        public virtual ICollection<Language> Language { get; set; }
        public virtual ICollection<Phone> Phone { get; set; }
        public virtual ICollection<Card> Card { get; set; }
        public virtual Defaults Defaults { get; set; }
    }
}
