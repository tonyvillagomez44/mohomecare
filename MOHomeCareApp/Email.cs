//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MOHomeCareApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class Email
    {
        public Email()
        {
            this.Defaults = new HashSet<Defaults>();
        }
    
        public int id { get; set; }
        public int worker_id { get; set; }
        public string address { get; set; }
        public int type_id { get; set; }
        public int flag_id { get; set; }
        public System.DateTime entereddate { get; set; }
        public System.DateTime lastupdate { get; set; }
        public int source_id { get; set; }
        public bool inactive { get; set; }
    
        public virtual Sources Sources { get; set; }
        public virtual Flag2 Flag2 { get; set; }
        public virtual Type2 Type2 { get; set; }
        public virtual List List { get; set; }
        public virtual ICollection<Defaults> Defaults { get; set; }
    }
}
