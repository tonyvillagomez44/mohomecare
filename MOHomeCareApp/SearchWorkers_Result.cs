//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MOHomeCareApp
{
    using System;
    
    public partial class SearchWorkers_Result
    {
        public int id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string ssn { get; set; }
        public string number_id { get; set; }
        public string street { get; set; }
    }
}
