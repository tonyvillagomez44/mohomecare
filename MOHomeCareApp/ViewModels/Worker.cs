﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MOHomeCareApp.ViewModels
{
    public class WorkerSummaryReport
    {
        
        //Global View
        public int WorkerId { get; set;}
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string SSN { get; set; }
        public int? VanId { get; set; }
        public string RegisteredToVote { get; set; }
        public DateTime? LastUpdate { get; set; }
        public DateTime? CreationDate { get; set; }
        public string UnionStatus { get; set; }
        public string SourceCard { get; set; }
        public List<SummaryListByWorker_Result> sumWorkerList { get; set; }

        //Address View
        public int? addressId { get; set; }
        public string addressType { get; set; }
        public string addressFlag { get; set; }
        public string addressStreet { get; set; }
        public string addressCity { get; set; }
        public string addressState { get; set; }
        public int? addressZip { get; set; }
        public DateTime? addressEnteredDate { get; set; }
        public DateTime? addressLastUpdate { get; set; }
        public string addressSource { get; set; }
        public bool? addressInactive { get; set; }
        public List<SummaryAddressByWorker_Result> sumAddressList { get; set; }

        //Phone View
        public int phoneId { get; set;}
        public string phoneNumeber { get; set; }
        public string phoneType { get; set; }
        public string phoneFlag { get; set; }
        public bool? phoneSms { get; set; }
        public DateTime? phoneEnteredDate { get; set; }
        public DateTime? phoneLastUpdate { get; set; }
        public string phoneSource { get; set; }
        public bool? phoneInactive { get; set; }
        public List<SummaryPhoneByWorker_Result> sumPhoneList { get; set; }

        //Email View
        public int emailId { get; set; }
        public string emailAddress { get; set; }
        public string emailType { get; set; }
        public string emailFlag { get; set; }
        public DateTime? emailEnteredDate { get; set; }
        public DateTime? emailLastUpdate { get; set; }
        public string emailSource { get; set; }
        public bool? emailInactive { get; set; }
        public List<SummaryEmailByWorker_Result> sumEmailList { get; set; }

        //Language View
        public int langId { get; set; }
        public string langName { get; set; }
        public string langFlag { get; set; }
        public DateTime? langEnteredDate { get; set; }
        public DateTime? langLastUpdate { get; set; }
        public string langSource { get; set; }
        public bool? langInactive { get; set; }
        public List<SummaryLanguageByWorker_Result> sumLangList { get; set; }

        public IEnumerable<SummaryListByWorker_Result> WorkerPersonalInfo { get; set; }
        public string windowView { get; set; }
        public string windowsOption { get; set; }
    }


    public class WorkerSearchResult
    {
        public List<SearchWorkers_Result> searchResults { get; set; }
    }

    public class WorkerSearch
    {
        [Display(Name = "Last Name"), RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Invalid Last Name"), Required]
        public string vmLastName { get; set; }
        [Display(Name = "First Name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Invalid First Name"), Required]
        public string vmFirstName { get; set; }
        [Display(Name = "SSN"), RegularExpression("^[0-9 ]*$", ErrorMessage = "Invalid SSN Last Four Digits "),
            StringLength(4, ErrorMessage = "Only SSN Last Four Digits")]
        public string vmSSN { get; set; }
        [Display(Name = "Address")]
        public string vmHomeAddress { get; set; }
        [Display(Name = "Phone"), StringLength(10, ErrorMessage = "Phone Number Are Only 10 Digits"),
            RegularExpression("^[0-9 ]*$", ErrorMessage = "Zipcodes Are Numeric"), DataType(DataType.PhoneNumber)]
        public string vmHomePhone { get; set; }

    }

    public class WorkerDataEntry
    {
        public SelectList vmDataSource { get; set; }
        [Display(Name = "Source")]
        public int? vmDataSourceId { get; set; }
        public string vmDataSourceName { get; set; }

        [Display(Name = "VAN Id"), RegularExpression("^[0-9 ]*$", ErrorMessage = "VAN ID Must Be Numeric")]
        public int? vmVanId { get; set; }


        [Display(Name="Last Name"), RegularExpression("^[a-zA-Z ]*$", ErrorMessage="Invalid Last Name"), Required]
        public string vmLastName { get; set; }
        [Display(Name = "First Name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Invalid First Name"), Required]
        public string vmFirstName { get; set; }
        [Display(Name = "Middle Name")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Invalid Middle Name")]
        public string vmMiddleName { get; set; }
        [Display(Name = "Date Of Birth")]
        public string vmDOB { get; set; }
        [Display(Name = "SSN"), RegularExpression("^[0-9 ]*$", ErrorMessage = "Invalid SSN Last Four Digits "),
            StringLength(4, ErrorMessage = "Only SSN Last Four Digits")]
        public string vmSSN { get; set; }
        [Range(1, 12, ErrorMessage = "Invalid DOB Month, Range Must Be Between 1 and 12")]
        public string vmDOBMonth { get; set; }
        [Range(1, 31, ErrorMessage = "Invalid DOB Day, Range Must Be Between 1 and 31")]
        public string vmDOBDay { get; set; }
        [Range(1925, 2025, ErrorMessage = "Invalid DOB Year, Range Must Be Between 1925 and 2025")]
        public string vmDOBYear { get; set; }

        [Display(Name = "Address")]
        public string vmHomeAddress { get; set; }
        [Display(Name = "City"), RegularExpression("^[a-zA-Z ]*$", ErrorMessage="Invalid City Name")]
        public string vmHomeCity { get; set; }
        public int? vmHomeCityId { get; set; }
        
        public SelectList vmHomeState { get; set; }
        [Display(Name = "State")]
        public int? vmHomeStateId { get; set; }
        public string vmHomeStateName { get; set; }

        [Display(Name = "Zip"), RegularExpression("^[0-9 ]*$", ErrorMessage = "Zipcodes Are Numeric")]
        public string vmHomeZip { get; set; }
        [Display(Name = "Phone"), StringLength(10,ErrorMessage="Phone Number Are Only 10 Digits"),
            RegularExpression("^[0-9 ]*$", ErrorMessage = "Zipcodes Are Numeric"), DataType(DataType.PhoneNumber)]
        public string vmHomePhone { get; set; }
        [Display(Name = "Email"), RegularExpression("[a-z0-9][-a-z0-9.!#$%&'*+-=?^_`{|}~\\/]+@([-a-z0-9]+\\.)+[a-z]{2,5}$", ErrorMessage = "Zipcodes Are Numeric"),
            DataType(DataType.EmailAddress)]
        public string vmHomeEmail { get; set; }
        [Display(Name = "Mobile"), StringLength(10,ErrorMessage="Phone Number Are Only 10 Digits"), 
            RegularExpression("^[0-9 ]*$", ErrorMessage = "Zipcodes Are Numeric")]
        public string vmHomeMobile { get; set; }
        [Display(Name = "SMS")]
        public bool vmHomeMobileSms { get; set; }

        
        public SelectList vmHomeLanguage { get; set; }
        [Display(Name = "Language")]
        public int? vmHomeLanguageId { get; set; }
        public string vmHomeLanguageName { get; set; }

        [Display(Name = "Registered To Vote")]
        public bool vmHomeRegVote { get; set; }

        [Display(Name = "Mail Address")]
        public string vmMailAddress { get; set; }
        [Display(Name = "Mail City")]
        public string vmMailCity { get; set; }
        
        public SelectList vmMailState { get; set; }
        [Display(Name = "Mail State")]
        public int? vmMailStateId { get; set; }
        public string vmMailStateName { get; set; }

        /*not in use yet
        public int? dropDownId { get; set; }
        public string dropDownName { get; set; }*/

        [Display(Name = "Mail Zip"), RegularExpression("^[0-9 ]*$", ErrorMessage = "Zipcodes Are Numeric")]
        public string vmMailZip { get; set; }
        [Display(Name = "Alternate Email")]
        public string vmAltEmail { get; set; }
        [Display(Name = "Alternate Phone")]
        public string vmAltPhone { get; set; }

        public SelectList vmUnionSigned { get; set; }
        [Display(Name = "Union Card Signed")]
        public int? vmUnionSignedId { get; set; }
        public string vmUnionSignedName { get; set; }


        [Display(Name = "Card Signed Date")]
        public string vmUnionSignedDate { get; set; }
        [Display(Name = "Card Received Date")]
        public string vmUnionReceived { get; set; }

        //[Display(Name = "Alternate Phone")]
        //public string vmCareOther { get; set; }
        [Display(Name = "CARE Received Date")]
        public string vmCareReceived { get; set; }

        public SelectList vmCareSigned { get; set; }
        [Display(Name = "CARE Card Signed")]
        public int? vmCareSignedId { get; set; }
        public string vmCareSignedName { get; set; }


        [Display(Name = "CARE Signed Date")]
        public string vmCareSignedDate { get; set; }

        [Display(Name = "CARE Amount")]
        public decimal? vmCareAmount{ get; set; }


    }
}