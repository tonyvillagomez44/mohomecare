﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MOHomeCareApp.ViewModels
{
    public class DataEntry
    {
    }

    public class ListOfCities
    {
        public int cityId { get; set; }
        public string cityName { get; set; }
    }

    public class ListOfZips
    {
        public int zipId { get; set; }
    }

    public class ListOfLanguages
    {
        public int langId { get; set; }
        public string langName { get; set; }
    }
}